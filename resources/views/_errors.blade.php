@if ($errors->any())
<div class="alert alert-danger">
    @foreach ($errors->all() as $msg)
    <div>{{ $msg }}</div>
    @endforeach    
</div>
@endif

@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif

@if (session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
@endif