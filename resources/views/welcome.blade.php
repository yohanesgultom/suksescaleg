@extends('layouts.public')

@section('title', __('Welcome'))

@section('styles')
{{-- <link rel="stylesheet" href="{{ asset('jquery.bxslider/jquery.bxslider.min.css') }}"> --}}
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.default.min.css">
<style>
.selectize-input {
    padding: 13px 8px !important;
}
</style>
@endsection

@section('scripts')
{{-- <script src="{{ asset('jquery.bxslider/jquery.bxslider.min.js') }}" type="text/javascript"></script> --}}
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js" type="text/javascript"></script>
<script src="{{ asset('flatpack/js/welcome.js') }}" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" type="text/javascript"></script>

<script>
const provinces = {!! json_encode($provinces) !!};

const changeIfSelected = function (selector) {
    if ($(selector).children('option:selected').length > 0) {
        $(selector).change()
    }
}

const isCaptchaChecked = function () {
  return grecaptcha && grecaptcha.getResponse().length !== 0;
}

$(document).ready(function () {

    $(".datepicker").flatpickr({
        defaultDate: new Date(),
        minDate: new Date(),
    })

    $('select[name=partai]').selectize()

    let selectizeKab = $('select[name=kabupaten_kota]').selectize({
        onChange: function (value) {
            let dpt = this.getOption(value).data('dpt')
            $('input[name=jumlah_dpt]').val(dpt)
        },
        render: {
            option: function(data, escape) {
                return `<div class="option" data-dpt="${data.dpt}" data-value="${escape(data.value)}">${escape(data.text)}</div>`
            },
        },
    })

    $('select[name=propinsi]').selectize({
        onChange: function (value) {
            let selectKab = $('select[name=kabupaten_kota]')
            selectKab.empty()
            let kabMap = provinces[value]
            if (kabMap) {
                for(let kab in kabMap) {
                    let total = kabMap[kab].toLocaleString('id')                    
                    let option = $('<option>').attr('value', kab).attr('data-dpt', total).text(kab)
                    selectKab.append(option)
                    selectizeKab[0].selectize.addOption({value: kab, text: kab, dpt: total})
                }
            }            
            selectizeKab[0].selectize.refreshOptions(true)
        }
    })    

    // $('select[name=propinsi]').on('change', function () {
    //     let selectKab = $('select[name=kabupaten_kota]')
    //     let defaultOption = selectKab.find('option').first()
    //     selectKab.empty().append(defaultOption)        
    //     let kabMap = provinces[this.value]
    //     if (kabMap) {
    //         for(let kab in kabMap) {
    //             let total = kabMap[kab].toLocaleString('id')
    //             let option = $('<option>').attr('value', kab).attr('data-dpt', total).text(kab)
    //             selectKab.append(option)
    //         }
    //     }
    // })    

    // $('select[name=kabupaten_kota]').on('change', function () {
    //     let dpt = $(this).children('option:selected').data('dpt') || ''
    //     $('input[name=jumlah_dpt]').val(dpt)
    // })
    
    $('#try-sms-form').on('submit', function() {
        event.preventDefault()
        $('button', this).attr('disabled', true)
        let data = $(this).serialize()
        let url = $(this).attr('action')
        $.post(url, data).done(function(res) {
            alert('SMS berhasil terkirim')
        }).fail(function (err) {
            console.error(err)
            $('button', '#try-sms-form').removeAttr('disabled')
            let msg = err.responseText || err.statusText
            alert('SMS gagal dikirim. Error: ' + msg)
        })
        // required for firefox
        return false
    });

    $('textarea[name=wording]').on('keyup', function() {        
        let counter = $('#wording_counter')        
        let maxChar = $(this).attr('maxlength')
        let remaining = maxChar - this.value.length
        let label = counter.text()
        label = counter.text().substr(0, label.lastIndexOf(' '))
        counter.text(label + ' ' + remaining)
    })
    .keyup() // trigger on load

    $('#order-form form').on('submit', function() {
        if (!isCaptchaChecked()) {
            alert('Please check reCaptcha')
            return false
        }
        event.preventDefault()
        let defaultText = $('#submit_btn', this).text()
        $('#submit_btn', this).text('Processing...')
        $('#submit_btn', this).attr('disabled', true)
        let data = $(this).serialize()
        let url = $(this).attr('action')
        let form = this
        $.post(url, data).done(function(res) {
            alert('Pengiriman berhasil')
            if (res.redirectURL) {                
                window.location.replace(res.redirectURL)
            }
        }).fail(function (err) {
            console.error(err)
            let data = JSON.parse(res)
            $('#submit_btn', form).text(defaultText)
            $('#submit_btn', form).attr('disabled', true)
            let msg = data.error || err.responseText || err.statusText
            alert('Pengiriman gagal. Silahkan coba lagi. Error: ' + msg)
        })
        // required for firefox
        return false
    })    

    // handle reload with input
    changeIfSelected('select[name=propinsi]')
    changeIfSelected('select[name=kabupaten_kota]')
})
</script> 
@endsection

@section('header')
<div class="container">

    @if (! empty($sliders))
    <ul id="bxslider-header">
        @foreach ($sliders as $slider)
        <li data-img="/storage/{{ $slider->path }}">
            <div class="sixteen columns margin_bottom_50 padding_top_60">
                <div class="twelve offset-by-two columns">
                    <div class="center_text big_padding">
                        <p class="big_title bold_text editContent" style="outline: none; cursor: default;">
                            {{ $slider->title }}
                        </p>
                        <p class="big_text normal_gray editContent" style="outline: none; cursor: default;">
                            {!! $slider->description !!}
                        </p>
                        <a href="{{ $slider->btn_url }}" class="pix_button pix_button_line white_border_button bold_text big_text btn_big">
                            <i class="pi pixicon-paper"></i>
                            <span>{{ $slider->btn_text }}</span>
                        </a>
                    </div>
                </div>
            </div>
        </li>
        @endforeach
    </ul>
    @endif

    <div class="center_text">
        <a href="#try" class="intro_arrow">
            <i class="pi pixicon-arrow-down" style="outline: none; cursor: default;"></i>
        </a>
    </div>
</div>
@endsection

@section('content')

<!-- Try -->
<div class="pixfort_normal_1" id="try">
    <div class="page_style pix_builder_bg">
        <div class="container">
            <div class="sixteen columns context_style">
                <div class="title_style">
                    <span class="editContent">
                        <span class="pix_text">{{ __('Coba SMS Gratis!') }}</span>
                    </span>
                </div>
                <div class="email_subscribe">
                    <form id="try-sms-form" action="{{ route('page.send.sms') }}" pix-confirm="hidden_pix_1" class="contact_form form_no_padding">
                        {{ csrf_field() }}
                        <input type="number" name="sms_number" class="pix_text" placeholder="081345678910" required="">
                        <button type="submit" class="subscribe_btn pix_text" id="try-sms">
                            <span class="editContent">{{ __('Kirim SMS') }}</span>
                        </button>
                        <div id="result"></div>
                    </form>
                </div>
                <div class="note_st">
                    <span class="editContent">
                        <span class="pix_text"></span>
                    </span>
                </div>
            </div>
        </div><!-- container -->
    </div>
</div>

<div class="pixfort_shop_7" id="why-us">
    <div class="new_story pix_builder_bg" style="outline: none; cursor: default; background: url({{ asset('images/why_bg.jpg') }}); background-size: cover;">
        <div class="container">
            <div class="sixteen columns">
                <div class="ten columns alpha">
                    <div class="zone_left">
                        <div class="text_st">
                            <h2>{{ __('Kenapa Kampanye SMS?') }}</h2>
                            <br>
                            @if (!empty($why_sms))
                            @foreach ($why_sms as $val)
                            <li class="subject_st editContent">{{ $val }}</li>
                            @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="steps">
    <div class="light_gray_bg big_padding pix_builder_bg">
        <h2 class="pix_text center_text">{{ __('Tahapan Pemesanan Kampanye SMS') }}</h2>
        <br>
        <div class="container">
            <div class="sixteen columns">
                @if (!empty($order_steps))
                @foreach ($order_steps as $step)
                <div class="four columns onethird_style alpha">
                    <div class="f1_box">                        
                        <div class="big_icon light_blue">
                            <img height="100px" src="{{ asset($step->icon) }}"/>
                            {{-- <span class="pi pixicon-profile-male"></span> --}}
                        </div>
                        <div class="center_text">
                            <span class="normal_title bold_text"><span class="editContent"><span class="pix_text">{{ $step->title }}</span></span></span><br>
                            <span class="editContent normal_text light_gray">
                                <span class="pix_text">{{ $step->body }}</span>
                            </span>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif
            </div>
        </div>
    </div>
</div>


<!-- order form -->
<div class="pixfort_gym_13 pix_builder_bg dark" id="order" style="outline: none; cursor: default; background-image: linear-gradient(rgba(0,0,0,0.50),rgba(0,0,0,0.50)), url({{ asset('flatpack/images/main/bg-form2.jpg') }})">
    <div class="join_us_section">
        <h2 class="pix_text center_text">{{ __('Formulir Pemesanan Kampanye SMS') }}</h2>

        <div class="pix_form_area">
            <div id="order-form" class="substyle pix_builder_bg form" style="outline: none; cursor: default;">

                @include('flash::message')

                <form action="{{ route('page.order') }}" class="container" method="post" pix-confirm="hidden_pix_13">
                    
                    {{ csrf_field() }}

                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="sixteen columns">
                            <label>{{ __('Nama Caleg') }}</label>
                            <input type="text" name="nama_caleg" placeholder="Nama calon legislatif" class="pix_text" required>
                        </div>                        
                    </div>

                    <div class="row">
                        <div class="eight columns">
                            <label>{{ __('Caleg') }}</label>
                            <select name="caleg" required>
                                <option value="DPR RI">DPR RI</option>
                                <option value="DPD RI">DPD RI</option>
                                <option value="DPRD Provinsi">DPRD Provinsi</option>
                                <option value="DPRD Kabupaten/Kota">DPR Kabupaten/Kota</option>
                            </select>
                        </div>
                        <div class="eight columns">
                            <label>{{ __('Partai') }}</label>
                            <select name="partai" required>
                                <option value="PARTAI ACEH">PARTAI ACEH</option>
                                <option value="PARTAI AMANAT NASIONAL">PARTAI AMANAT NASIONAL</option>
                                <option value="PARTAI BERKARYA">PARTAI BERKARYA</option>
                                <option value="PARTAI BULAN BINTANG">PARTAI BULAN BINTANG</option>
                                <option value="PARTAI DAERAH ACEH">PARTAI DAERAH ACEH</option>
                                <option value="PARTAI DEMOKRASI INDONESIA PERJUANGAN">PARTAI DEMOKRASI INDONESIA PERJUANGAN</option>
                                <option value="PARTAI DEMOKRAT">PARTAI DEMOKRAT</option>
                                <option value="PARTAI GERAKAN INDONESIA RAYA">PARTAI GERAKAN INDONESIA RAYA</option>
                                <option value="PARTAI GERAKAN PERUBAHAN INDONESIA">PARTAI GERAKAN PERUBAHAN INDONESIA</option>
                                <option value="PARTAI GOLONGAN KARYA">PARTAI GOLONGAN KARYA</option>
                                <option value="PARTAI HATI NURANI RAKYAT">PARTAI HATI NURANI RAKYAT</option>
                                <option value="PARTAI KEADILAN DAN PERSATUAN INDONESIA">PARTAI KEADILAN DAN PERSATUAN INDONESIA</option>
                                <option value="PARTAI KEADILAN SEJAHTERA">PARTAI KEADILAN SEJAHTERA</option>
                                <option value="PARTAI KEBANGKITAN BANGSA">PARTAI KEBANGKITAN BANGSA</option>
                                <option value="PARTAI NANGGROE ACEH">PARTAI NANGGROE ACEH</option>
                                <option value="PARTAI NASDEM">PARTAI NASDEM</option>
                                <option value="PARTAI PERSATUAN PEMBANGUNAN">PARTAI PERSATUAN PEMBANGUNAN</option>
                                <option value="PARTAI SIRA">PARTAI SIRA</option>
                                <option value="PARTAI SOLIDARITAS INDONESIA">PARTAI SOLIDARITAS INDONESIA</option>
                                <option value="PERSATUAN INDONESIA">PERSATUAN INDONESIA</option>                                
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="twelve columns">
                            <label>{{ __('Lokasi Kampanye') }}</label>
                        </div>                        
                    </div>

                    <div class="row">
                        <div class="six columns">
                            <label>{{ __('Propinsi') }}</label>
                            <select name="propinsi" required>
                                <option value="">-- Please Select --</option>
                                @if (!empty($provinces))
                                @foreach ($provinces as $key => $val)
                                <option value="{{ $key }}">{{ $key }}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="seven columns">
                            <label>{{ __('Kabupaten Kota') }}</label>
                            <select name="kabupaten_kota" required>
                                <option value="">-- Please Select --</option>
                            </select>
                        </div>                        
                        <div class="three columns">
                            <label>{{ __('Jumlah DPT') }}</label>
                            <input type="text" name="jumlah_dpt" class="pix_text" readonly>
                        </div>                        
                    </div>

                    <div class="row">
                        <div class="twelve columns">
                            <label>{{ __('Pesan Paket Kampanye SMS') }}</label>
                        </div>                        
                    </div>

                    <div class="row">
                        <div class="ten columns">
                            <label>{{ __('Paket & Harga') }}</label>
                            <select name="paket" required>
                                @if (!empty($packages))
                                @foreach ($packages as $name => $price)
                                <option value="{{ $name }}">{{ $name }} - Rp {{ number_format($price, 0, '', '.') }}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="six columns">
                            <label>{{ __('Tanggal Kampanye SMS') }}</label>
                            <input type="text" name="tanggal" class="pix_text datepicker" readonly required>
                        </div>
                    </div>

                    <div class="row">
                        <div class="sixteen columns">
                            <label id="wording_counter">{{ __('Teks Kampanye SMS') }} {{ __('(Sisa karakter)') }}: </label>
                            <textarea name="wording" rows="2" placeholder="Write your campaign text here" maxlength="160" minlength="5" required></textarea>
                        </div>
                    </div>

                    <div class="row">
                        <div class="twelve columns">
                            <label>{{ __('Info Klien') }}</label>
                        </div>                        
                    </div>

                    <div class="row">
                        <div class="eight columns">
                            <label>{{ __('Nama') }}</label>
                            <input type="text" name="nama" placeholder="Your name" class="pix_text" required>
                        </div>                        
                        <div class="eight columns">
                            <label>{{ __('Email') }}</label>
                            <input type="email" name="email" placeholder="username@hostname.com" class="pix_text">
                        </div>
                    </div>

                    <div class="row">
                        <div class="six columns">
                            <label>{{ __('No HP') }}</label>
                            <input type="text" name="no_hp" pattern="^08\d{6,10}$" placeholder="081345678910" class="pix_text" required>
                        </div>                        
                        <div class="six columns">
                            <label>{{ __('No WhatsApp') }}</label>
                            <input type="text" name="no_whatsapp" pattern="^08\d{6,10}$" placeholder="081345678910" class="pix_text">
                        </div>                        
                        <div class="four columns">
                            <label>{{ __('ID Reseller') }}</label>
                            <input type="text" name="reseller_id" placeholder="" class="pix_text">
                        </div>                                                
                    </div>

                    <div class="row">
                        <div class="twelve columns">
                            {!! Recaptcha::render() !!}                            
                            <br>
                            <span class="send_btn">
                                <button id="submit_btn" type="submit" class="slow_fade editContent pix_text" style="outline: none">
                                    <span class="editContent">{{ __('Kirim Pesanan') }}</span>
                                </button>
                            </span>
                        </div>
                    </div>

                </form>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
@endsection
