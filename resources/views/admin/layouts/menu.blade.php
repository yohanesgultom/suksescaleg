<li class="header">{{ __('MAIN NAVIGATION') }}</li>

<li class="{{ Request::is('admin/orders*') ? 'active' : '' }}">
    <a href="{!! route('admin.orders.index') !!}"><i class="fa fa-shopping-cart"></i><span>Orders</span></a>
</li>

<li class="{{ Request::is('admin/sliders*') ? 'active' : '' }}">
    <a href="{!! route('admin.sliders.index') !!}"><i class="fa fa-edit"></i><span>Sliders</span></a>
</li>

<li class="{{ Request::is('admin/variables*') ? 'active' : '' }}">
    <a href="{!! route('admin.variables.index') !!}"><i class="fa fa-cog"></i><span>Variables</span></a>
</li>

<li class="{{ Request::is('admin/admins*') ? 'active' : '' }}">
    <a href="{!! route('admin.admins.index') !!}"><i class="fa fa-user"></i><span>Admins</span></a>
</li>
