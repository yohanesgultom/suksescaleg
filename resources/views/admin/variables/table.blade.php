<div class="table-responsive">
<table class="table" id="variables-table">
    <thead>
        <tr>
            <th>Key</th>
            <th>Value</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($variables as $variable)
        <tr>
            <td>{!! $variable->key !!}</td>
            <td>{!! str_limit($variable->value) !!}</td>
            <td>
                <div class='btn-group'>
                    <a href="{!! route('admin.variables.edit', [$variable->key]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</div>