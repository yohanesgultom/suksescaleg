<table class="table table-responsive" id="users-table">
    <thead>
        <th>Username</th>
        <th>Created At</th>
        <th>Updated At</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($admins as $admin)
        <tr>
            <td>{!! $admin->username !!}</td>
            <td>{!! $admin->created_at !!}</td>
            <td>{!! $admin->updated_at !!}</td>
            <td>
                {!! Form::open(['route' => ['admin.admins.destroy', $admin->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.admins.edit', [$admin->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    @if ($admin->id != Auth::user()->id)
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    @endif
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
