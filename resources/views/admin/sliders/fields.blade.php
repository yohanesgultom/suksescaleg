<!-- Title field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Details Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Button text field -->
<div class="form-group col-sm-6">
    {!! Form::label('btn_text', 'Button Text:') !!}
    {!! Form::text('btn_text', null, ['class' => 'form-control']) !!}
</div>

<!-- Button URL field -->
<div class="form-group col-sm-6">
    {!! Form::label('btn_url', 'Button URL:') !!}
    {!! Form::text('btn_url', null, ['class' => 'form-control']) !!}
</div>

<!-- Path Field -->
@if (!empty($slider))
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('path', 'Image:') !!}
    <div>
      <img src="/storage/{{ $slider->path }}" height="200px" />
    </div>
</div>

<div class="form-group col-sm-6">
    {!! Form::label('image', 'Supported types: .png, .jpg, .jpeg, max. size: 650KB)') !!}
    {!! Form::file('image', ['class' => 'form-control']) !!}
</div>
@endif

@if (empty($slider))
<div class="form-group col-sm-6">
    {!! Form::label('image', 'Image (supported types: .png, .jpg, .jpeg, max. size: 650KB)') !!}
    {!! Form::file('image', ['class' => 'form-control']) !!}
</div>
@endif

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.sliders.index') !!}" class="btn btn-default">Cancel</a>
</div>
