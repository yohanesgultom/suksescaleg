<div class="form-group col-sm-6">
    {!! Form::label('nama_caleg', 'Nama Caleg:') !!}
    {!! Form::text('nama_caleg', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('caleg', 'Caleg:') !!}
    {!! Form::text('caleg', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('partai', 'Partai:') !!}
    {!! Form::text('partai', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-4">
    {!! Form::label('propinsi', 'Propinsi:') !!}
    {!! Form::text('propinsi', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('kabupaten_kota', 'Kabupaten Kota:') !!}
    {!! Form::text('kabupaten_kota', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-2">
    {!! Form::label('jumlah_dpt', 'Jumlah DPT:') !!}
    {!! Form::text('jumlah_dpt', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-4">
    {!! Form::label('paket', 'Paket:') !!}
    {!! Form::text('paket', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-4">
    {!! Form::label('harga', 'Harga:') !!}
    {!! Form::text('harga', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-4">
    {!! Form::label('tanggal', 'Tanggal:') !!}
    {!! Form::text('tanggal', null, ['class' => 'form-control datepicker']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('wording', 'Wording:') !!}
    {!! Form::textarea('wording', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('nama', 'Nama:') !!}
    {!! Form::text('nama', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('email', 'E-mail:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('no_hp', 'No HP:') !!}
    {!! Form::text('no_hp', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('no_whatsapp', 'No WhatsApp:') !!}
    {!! Form::text('no_whatsapp', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::select('status', \App\Models\ORDER::STATUSES, null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.orders.index') !!}" class="btn btn-default">Cancel</a>
</div>
