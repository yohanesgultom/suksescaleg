<div class="table-responsive">
<table class="table" id="orders-table">
    <thead>
        <tr>
            <th>Kode</th>
            <th>Nama Caleg</th>
            <th>Partai</th>
            <th>Kabupaten Kota</th>
            <th>Paket</th>
            <th>Tanggal</th>
            <th>Created At</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($orders as $order)
        <tr>
            <td>{!! $order->code !!}</td>
            <td>{!! $order->nama_caleg !!}</td>
            <td>{!! $order->partai !!}</td>
            <td>{!! $order->kabupaten_kota !!}</td>
            <td>{!! $order->paket !!}</td>
            <td>{!! $order->tanggal->toDateString() !!}</td>
            <td>{!! $order->created_at->toDateString() !!}</td>
            <td>
                {!! Form::open(['route' => ['admin.orders.destroy', $order->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.orders.edit', [$order->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</div>