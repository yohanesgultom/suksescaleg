<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="{{ config('app.locale') }}"> <!--<![endif]-->
<head>
    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title') | {{ config('app.name') }}</title>

    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="x-ua-compatible" content="IE=9">
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <!--headerIncludes-->
    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.0/css/all.min.css">
    <link rel="stylesheet" href="{{ asset('flatpack/css/menu.css') }}">
    <link rel="stylesheet" href="{{ asset('flatpack/css/flat-ui-slider.css') }}">
    <link rel="stylesheet" href="{{ asset('flatpack/css/base.css') }}">
    <link rel="stylesheet" href="{{ asset('flatpack/css/skeleton.css') }}">
    <link rel="stylesheet" href="{{ asset('flatpack/css/landings.css') }}">
    <link rel="stylesheet" href="{{ asset('flatpack/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('flatpack/css/landings_layouts.css') }}">
    <link rel="stylesheet" href="{{ asset('flatpack/css/box.css') }}">
    <link rel="stylesheet" href="{{ asset('flatpack/css/pixicon.css') }}">
    <link rel="stylesheet" href="{{ asset('flatpack/css/animations.min.css') }}">
    <link rel="stylesheet" href="{{ asset('auto-complete/auto-complete.css') }}">

    <!--[if lt IE 9]>
    <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="{{ asset('flatpack/images/favicon.ico') }}">
    <link rel="apple-touch-icon" href="{{ asset('flatpack/images/apple-touch-icon.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('flatpack/images/apple-touch-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('flatpack/images/apple-touch-icon-114x114.png') }}">

    @yield('styles')
</head>
<body>
    <div id="page" class="page">
        <div class="header_nav_1 dark inter_3_bg pix_builder_bg" id="section_intro_3">
            <div class="header_style">
                <div class="container">
                    <div class="sixteen columns firas2">
                        <nav role="navigation" class="navbar navbar-white navbar-embossed navbar-lg pix_nav_1">
                            <div class="containerss">
                                <div class="navbar-header">
                                    <button data-target="#navbar-collapse-02" data-toggle="collapse" class="navbar-toggle" type="button">
                                        <span class="sr-only">Toggle navigation</span>
                                    </button>
                                    <div class="brand"><a href="{{ route('page.index') }}">{{ config('app.name') }}</a></div>
                                </div>
                                <div id="navbar-collapse-02" class="collapse navbar-collapse">
                                    <ul class="nav navbar-nav navbar-right">
                                        <li class="propClone"><a href="{{ route('page.index') }}#try">{{ __('Try SMS') }}</a></li>
                                        <li class="propClone"><a href="{{ route('page.index') }}#why-us">{{ __('Why SMS') }}</a></li>
                                        <li class="propClone"><a href="{{ route('page.index') }}#steps">{{ __('How to Order') }}</a></li>
                                        <li class="propClone"><a href="{{ route('page.index') }}#contact">{{ __('Contact Us') }}</a></li>
                                        <li class="propClone"><a href="{{ route('page.index') }}#order" style="color: yellow;">{{ __('Order') }}</a></li>
                                    </ul>
                                </div><!-- /.navbar-collapse -->
                            </div><!-- /.container -->
                        </nav>
                    </div>
                </div><!-- container -->
            </div>

            @yield('header')

        </div>

        @yield('content')

        <div class="pixfort_party_15" id="section_party_4">
                <div class="foot_st pix_builder_bg">
                   <div class="container">
                        <div class="sixteen columns">
                            <div class="seven columns alpha">
                                <span class="editContent">
                                    <span class="pix_text">
                                        <span class="rights_st"> All rights reserved Copyright © 2019
                                            <span class="pixfort_st">{{ config('app.name') }}</span>
                                        </span>
                                    </span>
                                </span>
                            </div>
                            <div class="nine columns omega ">
                                <div class="socbuttons">
                                   <div class="soc_icons pix_builder_bg">
                                        <ul class="bottom-icons">
                                            <li><a target="_blank" href="https://wa.me/6285782866340" style="font-size: 1.5em; font-weight: bolder; color: #74e48c;"><i class="fab fa-whatsapp"></i></li>
                                            <li><a target="_blank" href="https://www.instagram.com/suksescaleg" style="font-size: 1.5em; font-weight: bolder; color: #daa931;"><i class="fab fa-instagram"></i></a></li>
                                        </ul>
                                    </div>
                                    <div id="contact" class="likes_st">
                                        <span class="editContent">
                                            <span class="pix_text">{{ __('Contact us') }}:</span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>

<!-- JavaScript
================================================== -->
<script src="{{ asset('flatpack/js/jquery-1.8.3.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('flatpack/js/jquery.easing.1.3.js') }}" type="text/javascript"></script>
<script src="{{ asset('flatpack/js/jquery.common.min.js') }}" type="text/javascript" ></script>
<script src="{{ asset('flatpack/js/ticker.js') }}" type="text/javascript"></script>
<script src="{{ asset('flatpack/js/smoothscroll.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('flatpack/js/appear.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('flatpack/js/jquery.ui.touch-punch.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('flatpack/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('flatpack/js/bootstrap-switch.js') }}" type="text/javascript"></script>
<script src="{{ asset('flatpack/js/custom1.js') }}" type="text/javascript"></script>
<script src="{{ asset('flatpack/js/appear.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('flatpack/js/animations.js') }}" type="text/javascript"></script>

<script src="{{ asset('flatpack/js/public.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery.smooth-scroll.min.js') }}" type="text/javascript"></script>
<!-- Begin of Chaport Live Chat code -->
<script type="text/javascript">
(function(w,d,v3){ w.chaportConfig = { appId : '5c8b2c1e7a33291881542e07' }; if(w.chaport)return;v3=w.chaport={};v3._q=[];v3._l={};v3.q=function(){v3._q.push(arguments)};v3.on=function(e,fn){if(!v3._l[e])v3._l[e]=[];v3._l[e].push(fn)};var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://app.chaport.com/javascripts/insert.js';var ss=d.getElementsByTagName('script')[0];ss.parentNode.insertBefore(s,ss)})(window, document);
</script>
<!-- End of Chaport Live Chat code -->
@yield('scripts')

</body>
</html>
