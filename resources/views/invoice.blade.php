<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>{{ __('Invoice') }} | {{ config('app.name') }}</title>
  
  <style type="text/css">
    body {
      padding: 2em;
      font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    h1 {
      font-size: 45px;
      line-height: 45px;
      color: #333;
    }
    table{
      color: #111;
    }
    table.items {
      border-spacing: 0px;      
      border: 1px solid lightgray;
    }
    table.items td {      
      padding: 5px;
    }
    tfoot tr td{
      font-weight: bold;
    }
    .gray {
      background-color: lightgray;
    }
    .action {
      text-align: right;
    }
    button {
      background-color: #4CAF50; /* Green */
      border: none;
      color: white;
      padding: 15px 32px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
      font-size: 16px;
      cursor: pointer;
    }
  </style>
  
</head>
<body>
  <table width="100%">
    <tr>
      <td>
        <h1>{{ config('app.name') }}</h1>
        <div>
          <div>Jakarta, Indonesia</div>
          <div>WA +6285782866340</div>
        </div>
      </td>
    </tr>
  </table>
  
  <br/>
  
  <table width="100%">
    <tr>
      <td>
        <h3>Tagihan kepada</h3>
        <div>
          <div>Nama: {{ $order->nama }}</div>
          <div>No HP: {{ $order->no_hp }}</div>
          <div>Email: {{ $order->email }}</div>
        </div>
      </td>
      <td align="right">
        <div>
          <div>Invoice #{{ $order->code }}</div>
          <div>Tanggal cetak faktur: {{$order->created_at->isoFormat('Do MMMM YYYY, HH:mm') }} WIB</div>
        </div>
      </td>        
    </tr>
  </table>
  
  <br/>
  
  <table class="items" width="100%">
    <thead style="background-color: lightgray;">
      <tr>
        <th width="80%">Item</th>
        <th>Harga</th>
      </tr>
    </thead>
    <tbody>
      <tr>
      <td>{{ $order->paket }} {{ $order->kabupaten_kota }}</td>
        <td align="right">Rp {{ number_format($order->harga, 0, '', '.')  }}</td>
      </tr>
    </tbody>
    <tfoot>
      <tr>
        <td align="right">Total</td>
        <td align="right">Rp {{ number_format($order->harga, 0, '', '.')  }}</td>
      </tr>
    </tfoot>
  </table>
  
  <br>
  
  <table>
    <tr>        
      <td>
        <h4>Syarat & Ketentuan</h4>
        <div>{!! \Str::replaceFirst('?', $order->created_at->addHours(24)->isoFormat('Do MMMM YYYY, HH:mm').' WIB', $instructions) !!}</div>
      </td>
    </tr>
  </table>
  
  <br>
  
  @if ($download == true)
  <div class="action">
    <button onclick="location.href='{{ route('page.invoice.download', $order->code) }}';">{{ __('Download PDF') }}</button>
  </div>
  @endif  
</body>
</html>