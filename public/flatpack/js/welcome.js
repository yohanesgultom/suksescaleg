// jQuery Initialization
$(document).ready(function(){

    // header slider
    const style = 'linear-gradient(rgba(0,0,0,0.50),rgba(0,0,0,0.50)), black url({0}) fixed no-repeat center';
    $('#bxslider-header').bxSlider({
      auto: true,
      controls: false,
      onSliderLoad: function (currentIndex) {
        $('#section_intro_3')
          .css(
            'background',
            style.replace('{0}', $('li:last-child', this).attr('data-img'))
          );
      },
      onSlideBefore: function ($slideElement, oldIndex, newIndex) {
        $('#section_intro_3')
          .css(
            'background',
            style.replace('{0}', $slideElement.attr('data-img'))
          );
      }
    });

});
