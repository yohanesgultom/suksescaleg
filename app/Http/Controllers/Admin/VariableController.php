<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CreateVariableRequest;
use App\Http\Requests\UpdateVariableRequest;
use App\Repositories\VariableRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class VariableController extends AppBaseController
{
    /** @var  VariableRepository */
    private $variableRepository;

    public function __construct(VariableRepository $variableRepo)
    {
        $this->variableRepository = $variableRepo;
    }

    /**
     * Display a listing of the Variable.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->variableRepository->pushCriteria(new RequestCriteria($request));
        $variables = $this->variableRepository->paginate(10);

        return view('admin.variables.index')
            ->with('variables', $variables);
    }

    /**
     * Show the form for creating a new Variable.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.variables.create');
    }

    /**
     * Store a newly created Variable in storage.
     *
     * @param CreateVariableRequest $request
     *
     * @return Response
     */
    public function store(CreateVariableRequest $request)
    {
        $input = $request->all();

        $variable = $this->variableRepository->create($input);

        Flash::success('Variable saved successfully.');

        return redirect(route('admin.variables.index'));
    }

    /**
     * Display the specified Variable.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {        
        $variable = $this->variableRepository->findWithoutFail($id);

        if (empty($variable)) {
            Flash::error('Variable not found');

            return redirect(route('admin.variables.index'));
        }

        return view('admin.variables.show')->with('variable', $variable);
    }

    /**
     * Show the form for editing the specified Variable.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $variable = $this->variableRepository->findWithoutFail($id);

        if (empty($variable)) {
            Flash::error('Variable not found');

            return redirect(route('admin.variables.index'));
        }

        return view('admin.variables.edit')->with('variable', $variable);
    }

    /**
     * Update the specified Variable in storage.
     *
     * @param  int              $id
     * @param UpdateVariableRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVariableRequest $request)
    {
        $variable = $this->variableRepository->findWithoutFail($id);

        if (empty($variable)) {
            Flash::error('Variable not found');

            return redirect(route('admin.variables.index'));
        }

        $variable = $this->variableRepository->update($request->all(), $id);

        Flash::success('Variable updated successfully.');

        return redirect(route('admin.variables.index'));
    }

    /**
     * Remove the specified Variable from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $variable = $this->variableRepository->findWithoutFail($id);

        if (empty($variable)) {
            Flash::error('Variable not found');

            return redirect(route('admin.variables.index'));
        }

        $this->variableRepository->delete($id);

        Flash::success('Variable deleted successfully.');

        return redirect(route('admin.variables.index'));
    }
}
