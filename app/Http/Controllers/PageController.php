<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Slider;
use App\Models\Variable;
use App\Models\Order;
use App\Models\Trial;
use App\Jobs\SendSMS;
use Dompdf\Dompdf;
use Illuminate\Support\Facades\Mail;
use Str;

class PageController extends Controller
{    
    public function index()
    {
        $keys = ['WHY_SMS', 'ORDER_STEPS', 'PROVINCES', 'PACKAGES'];
        $variables = Variable::whereIn('key', $keys)->get()->pluck('value', 'key')->all();
        return view('welcome', [
            'sliders' => Slider::orderBy('id', 'asc')->get(),
            'why_sms' => json_decode($variables['WHY_SMS']),
            'order_steps' => json_decode($variables['ORDER_STEPS']),
            'provinces' => json_decode($variables['PROVINCES']),
            'packages' => json_decode($variables['PACKAGES']),
        ]);
    }

    public function order(Request $request)
    {
        $rules = Order::$rules;
        $rules['g-recaptcha-response'] = 'required|recaptcha';
        try {
            $request->validate($rules);
            $input = $request->except(['id', 'status', 'harga']);                        
            $packages_var = Variable::find('PACKAGES');
            if (empty($packages_var)) {
                return response()->json(['error' => 'Paket tidak ditemukan'], 400);
                // abort(404, __('Package not found'));
            }
            $packages = json_decode($packages_var->value, true);
            $input['harga'] = $packages[$input['paket']];
            $order = Order::create($input);
            
            // send SMS containing invoice URL to customer
            $url = route('page.invoice', $order->code);            
            $message = \Str::replaceFirst('?', $url, Variable::find('INVOICE_TEXT_NOTIF')->value);
            SendSMS::dispatch($order->no_hp, $message);

            // send order notif SMS to admin            
            $admin_phone_no = Variable::find('ADMIN_MOBILE_NO')->value;
            $order_url = route('admin.orders.edit', $order->id);
            $notif_message = "Order baru suksescaleg.id dari {$order->nama} ($order->no_hp) {$order_url}";
            SendSMS::dispatch($order->no_hp, $message);

            // send email
            if (!empty($order->email)) {
                Mail::to($order->email)->queue(new \App\Mail\Generic($message, __('Faktur kampanye SMS')));
            }
            // return redirect()->route('page.invoice', $order->code);    
            return response()->json(['redirectURL' => route('page.invoice', $order->code)]);
        } catch (\Exception $e) {
            \Log::error($e->getMessage(), ['exception' => $e]);
            // abort(500, $e->getMessage());
            return response()->json(['error' => $e->getMessage()], 500);
        }        
    }

    public function invoice($id)
    {
        $order = Order::where('code', $id)->first();
        if (empty($order)) {
            abort(404, __('Order not found').':'.$id);
        }
        $instructions = Variable::find('INVOICE_INSTRUCTIONS')->value;
        return view('invoice')
            ->with('download', true)
            ->with('order', $order)
            ->with('instructions', $instructions);
    }

    public function download($id)
    {
        $order = Order::where('code', $id)->first();
        if (empty($order)) {
            abort(404, __('Order not found').':'.$id);
        }
        $instructions = Variable::find('INVOICE_INSTRUCTIONS')->value;
        $view = view('invoice')
            ->with('download', false)
            ->with('order', $order)
            ->with('instructions', $instructions);

        // render pdf
        $dompdf = new Dompdf();
        $dompdf->loadHtml($view->render());
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        return $dompdf->stream('invoice.pdf');
    }

    public function sendSMS(Request $request)
    {
        $input = $request->validate([
            'sms_number' => 'required|digits_between:8,12',
        ]);
        $message = Variable::find('TRY_IT_SMS')->value;
        $ip = request()->ip();
        $code = Trial::generateCode($ip);
        $trial = Trial::find($code);
        if (!empty($trial)) {
            return response()->json(__('Sorry, you have reached trial limit'), 400);
        } else {
            Trial::create([
                'code' => $code,
                'mobileno' => $input['sms_number'],
                'ip' => $ip,
            ]);
        }
        try {
            $mtid = SendSMS::call_sms_api($input['sms_number'], $message);
            if ($mtid == '-200') {
                return response()->json('API error:'.$mtid, 500);
            }
            return response()->json(['MT_ID' => $mtid]);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();
            \Log::error("Get statements API error", [ 'response' => (array)$response ]);
            return response()->json($e->getMessage(), 500);
        } catch (\Exception $e) {
            \Log::error("Get statements API error: ".$e->getMessage());
            return response()->json($e->getMessage(), 500);
        }        
    }
}
