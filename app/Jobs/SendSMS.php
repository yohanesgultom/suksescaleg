<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use GuzzleHttp\Client;
use Str;
use Log;

class SendSMS implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $no;
    public $message;
    public $timeout;

    public function __construct($no, $message, $timeout = null)
    {
        $this->no = $no;
        $this->message = $message;        
        $this->timeout = $timeout;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $error_message = "Failed sending SMS to {$this->no}: {$this->message}";
        try {            
            $mtid = static::call_sms_api($this->no, $this->message, $this->timeout);
            if ($mtid == '-200') {
                Log::error("Failed sending SMS. MT ID: {$mtid}");
            }                
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();
            Log::error($error_message, [ 'response' => (array)$response ]);
        } catch (\Exception $e) {
            Log::error($error_message.". Exception: ".$e->getMessage());
        }
    }

    static function call_sms_api($mobileno, $message, $timeout = 5)
    {
        // validate 62 prefix
        if (!Str::startsWith($mobileno, '62')) {
            if (Str::startsWith($mobileno, '0')) {
                $mobileno = Str::replaceFirst('0', '62', $mobileno);
            } else {
                $mobileno = '62'.$mobileno;
            }            
        }
        $params = [
            'apiusername' => config('app.sms_api_username'),
            'apipassword' => config('app.sms_api_password'),
            'senderid' => config('app.name'),
            'mobileno' => $mobileno,
            'message' => $message,
            'languagetype' => 1,
        ];
        $client = new Client(['timeout' => $timeout,]);
        $response = $client->request('GET', config('app.sms_api_url'), ['query' => $params]);    
        return $response->getBody()->getContents();
    }    
}
