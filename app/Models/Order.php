<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    const STATUSES = [
        'NEW' => 'NEW',
        'PAID' => 'PAID',
        'DONE' => 'DONE',
        'CANCELED' => 'CANCELED',
    ];

    public $fillable = [
        'nama_caleg',
        'caleg',
        'partai',
        'propinsi',
        'kabupaten_kota',
        'jumlah_dpt',
        'paket',
        'harga',
        'tanggal',
        'wording',
        'nama',
        'email',
        'no_hp',
        'no_whatsapp',
        'reseller_id',
        'status',
    ];

    protected $dates = [
        'tanggal',
    ];

    public static $rules = [
        'nama_caleg' => 'required',
        'caleg' => 'required',
        'partai' => 'required',
        'propinsi' => 'required',
        'kabupaten_kota' => 'required',
        'jumlah_dpt' => 'required',
        'paket' => 'required',
        'tanggal' => 'required|date',
        'wording' => 'required|max:160',
        'nama' => 'required',
        'email' => 'email',
        'no_hp' => 'required|regex:/^08\d{6,10}$/i',        
    ];    

    public static function boot()
    {
        parent::boot();

        static::creating(function($model)
        {
            $model->jumlah_dpt = str_replace('.', '', $model->jumlah_dpt);
            do {
                $code = \Str::random(5);
            } while (Order::where('code', $code)->count() > 0);
            $model->code = $code;
        });
    }

}
