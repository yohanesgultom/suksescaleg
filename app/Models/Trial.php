<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Trial extends Model
{
    protected $primaryKey = 'code';
    
    protected $keyType = 'string';

    public $incrementing = false;  

    public $fillable = [
        'code',
        'mobileno',
        'ip',
    ];

    public static function generateCode($ip)
    {
        $date = Carbon::now()->format('Ymd');
        return "{$ip}_{$date}";
    }
}
