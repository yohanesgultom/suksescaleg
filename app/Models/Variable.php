<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Variable extends Model
{
    protected $primaryKey = 'key';
    
    protected $keyType = 'string';

    public $incrementing = false;    

    protected $fillable = [
        'key', 'value',
    ];    

    static $rules = [
        'key' => 'required',
    ];
}
