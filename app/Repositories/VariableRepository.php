<?php

namespace App\Repositories;

use App\Models\Variable;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class VariableRepository
 * @package App\Repositories
 * @version October 26, 2018, 4:46 am UTC
 *
 * @method Variable findWithoutFail($id, $columns = ['*'])
 * @method Variable find($id, $columns = ['*'])
 * @method Variable first($columns = ['*'])
*/
class VariableRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'key',
        'value'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Variable::class;
    }
}
