<?php

namespace App\Repositories;

use App\Admin;
use InfyOm\Generator\Common\BaseRepository;

class AdminRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'username',
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Admin::class;
    }
}
