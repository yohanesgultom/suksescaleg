<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Generic extends Mailable
{
    use Queueable, SerializesModels;

    public $tries = 3;

    public $body;
    public $subject;

    public function __construct($body, $subject)
    {
        $this->body = $body;
        $this->subject = $subject;
    }

    public function build()
    {
        return $this->view('emails.generic')->subject($this->subject);
    }
}
