<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code')->nullable();
            $table->string('nama_caleg');
            $table->string('caleg');
            $table->string('partai');
            $table->string('propinsi');
            $table->string('kabupaten_kota');
            $table->string('jumlah_dpt');
            $table->string('paket');
            $table->double('harga', 14, 2);
            $table->date('tanggal');
            $table->string('wording');
            $table->string('nama');
            $table->string('email')->nullable();
            $table->string('no_hp');
            $table->string('no_whatsapp')->nullable();
            $table->string('reseller_id')->nullable();
            $table->string('status')->default('NEW');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
