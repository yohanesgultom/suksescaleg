<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class SlidersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dir = public_path('images/slides');
        $sliders = [
          [
            'title' => 'SuksesCaleg.ID',
            'description' => 'Cara baru kampanye yang efektif dan efisien',
            'btn_text' => 'Lihat paket harga',
            'btn_url' => '#order',
            'filename' => 'PEMILU 2019.png',
          ],
          [
            'title' => 'SuksesCaleg.ID',
            'description' => 'Cara baru kampanye yang efektif dan efisien',
            'btn_text' => 'Lihat paket harga',
            'btn_url' => '#order',
            'filename' => 'campaign.jpg',
          ],
          [
            'title' => 'SuksesCaleg.ID',
            'description' => 'Cara baru kampanye yang efektif dan efisien',
            'btn_text' => 'Lihat paket harga',
            'btn_url' => '#order',
            'filename' => 'PILEG.png',
          ],
        ];

        for ($i = 0; $i < count($sliders); $i++) {
          $s = $sliders[$i];
          $file = new File($dir.DIRECTORY_SEPARATOR.$s['filename']);
          $sliders[$i]['path'] = Storage::disk('public')->putFile(\App\Models\Slider::storageDir, $file);
          $sliders[$i]['created_at'] = new \DateTime();
        }

        DB::table('sliders')->insert($sliders);
    }
}
