<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@index')->name('page.index');
Route::post('order', 'PageController@order')->name('page.order');
Route::get('invoice/{id}', 'PageController@invoice')->name('page.invoice');
Route::get('invoice/{id}/download', 'PageController@download')->name('page.invoice.download');
Route::post('sendsms', 'PageController@sendSMS')->name('page.send.sms');

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
// Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
// Route::post('register', 'Auth\RegisterController@register');
// Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
// Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
// Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
// Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');
// Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
// Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');
// Route::get('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');

Route::middleware(['auth'])->group(function () {
    Route::get('logout', 'Auth\LoginController@logout')->name('logout');
    Route::post('logout', 'Auth\LoginController@logout');
    Route::get('home', 'HomeController@index')->name('home');
});

Route::prefix('admin')->name('admin.')->group(function () {
    Route::get('login', 'Admin\AuthController@showLoginForm')->name('login');
    Route::post('login', 'Admin\AuthController@login');
});

Route::prefix('admin')->name('admin.')->middleware(['auth:admin'])->group(function () {
    Route::get('logout', 'Admin\AuthController@logout')->name('logout');
    Route::post('logout', 'Admin\AuthController@logout');    
    Route::get('/', 'Admin\HomeController@index')->name('home');
    Route::resource('admins', 'Admin\AdminController');
    Route::resource('sliders', 'Admin\SliderController');
    Route::resource('variables', 'Admin\VariableController');
    Route::resource('orders', 'Admin\OrderController');
});
